CC=gcc
CFLAGS=-std=c11 -m64 -Wall -Wextra -Wshadow -pedantic -Werror
all: avs_lab_4

avs_lab_4:	avs_lab_4.c
		$(CC) $(CFLAGS) main.c -o avs_lab_4 -lm

debug:		avs_lab_4.c
		$(CC) $(CFLAGS) -g main.c -o avs_lab_4 -lm

clean:
		rm avs_lab_4

