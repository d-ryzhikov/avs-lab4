#include <float.h>
#include <errno.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern int errno;

double *calculate_f(float *a, float *b, float *c, double *d, bool *overflow,
                    bool *underflow);

bool read_from_file(FILE *input, float *a, float *b, float *c, double *d);

bool parse_line_float(char *line, float *x);

bool parse_line_double(char *line, double *x);

void print_results(float *a, float *b, float *c, double *d, double *f);

int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("Usage:\n\t%s FILENAME\n", argv[0]);
        exit(EXIT_SUCCESS);
    }

    FILE *input = fopen(argv[1], "r");

    if (input == NULL) {
        fprintf(stderr, "Error opening file: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    float a[8] __attribute__((aligned(16)));
    float b[8] __attribute__((aligned(16)));
    float c[8] __attribute__((aligned(16)));
    double d[8] __attribute__((aligned(16)));

    if (!read_from_file(input, a, b, c, d)) {
        printf("Error reading from file: file contents are not valid.\n");
        exit(EXIT_FAILURE);
    }

    bool overflow = false;
    bool underflow = false;

    double *f = calculate_f(a, b, c, d, &overflow, &underflow);

    if (overflow) {
        printf("Overflow detected: input values are too big.\n");
    } else if (underflow) {
        printf("Underflow detected: input values are too small.\n");
    } else {
        print_results(a, b, c, d, f);
    }

    return 0;
}

double *calculate_f(float *a, float *b, float *c, double *d, bool *overflow,
                    bool *underflow)
{
    static double f[8] __attribute__((aligned(16)));

    __asm__ volatile (
    ".section .data\n\t"    
        "status: .long 0\n\t"
    ".section .text\n\t"    
        "xor %%rcx, %%rcx\n\t"
    "main_loop:\n\t"
        "cvtps2pd (%[c], %%rcx, 4), %%xmm0\n\t"     //xmm0=double(c[rcx,rcx+1])
        "cvtps2pd (%[b], %%rcx, 4), %%xmm1\n\t"     //xmm1=double(b[rcx,rcx+1])
        "addpd %%xmm1, %%xmm0\n\t"                  //xmm0=c+b[rcx,rcx+1]
        "cvtps2pd (%[a], %%rcx, 4), %%xmm1\n\t"     //xmm1=double(a[rcx,rcx+1])
        "mulpd %%xmm1, %%xmm0\n\t"                  //xmm0=a*(c+b)[rcx,rcx+1]
        "mulpd (%[d], %%rcx, 8), %%xmm0\n\t"        //xmm0=a*(c+b)*d[rcx,rcx+1]
        "stmxcsr status\n\t"                        //status=mxcsr
        "testb $8, status\n\t"                      //check overflow
        "jnz overflow\n\t"
        "testb $16, status\n\t"                     //check underflow
        "jnz underflow\n\t"
        "movaps %%xmm0, (%[f], %%rcx, 8)\n\t"       //load f[rcx,rcx+1]
        "cmp $6, %%rcx\n\t"
        "jae end\n\t"
        "add $2, %%rcx\n\t"
        "jmp main_loop\n\t"
    "overflow:\n\t"
        "movb $1, %[of]\n\t"
        "jmp end\n\t"
    "underflow:\n\t"
        "movb $1, %[uf]\n\t"
    "end:\n\t"
        : [of] "=m" (*overflow), [uf] "=m" (*underflow)
        : [a] "r" (a), [b] "r" (b), [c] "r" (c), [d] "r" (d), [f] "r" (f)
        : "rcx", "memory"
    );

    return f;
}

bool read_from_file(FILE *input, float *a, float *b, float *c, double *d)
{
    char line[256];

    if (fgets(line, 256, input) == NULL) {
        return false;
    }
    if (!parse_line_float(line, a)) {
        return false;
    }
    if (fgets(line, 256, input) == NULL) {
        return false;
    }
    if (!parse_line_float(line, b)) {
        return false;
    }
    if (fgets(line, 256, input) == NULL) {
        return false;
    }
    if (!parse_line_float(line, c)) {
        return false;
    }
    if (fgets(line, 256, input) == NULL) {
        return false;
    }
    if (!parse_line_double(line, d)) {
        return false;
    }
    return true;
}

bool parse_line_float(char *line, float *x)
{
    uint8_t c = 0;
    int8_t i = 0;
    int8_t sign;
    float result; 
    uint8_t dot_pos;

    while (((line[c] != '\n') || (line[c] != EOF)) && (i < 8)) {
        result = 0;
        dot_pos = 0;
        sign = 1;
        while ((line[c] == ' ') || (line[c] == '\t')) {
            ++c;
        }
        if (line[c] == '-') {
            sign = -1;
            ++c;
        }
        if (((line[c] < '0') || (line[c] > '9')) && (line[c] != '.')) {
            return false;
        }
        while ((line[c] >= '0') && (line[c] <= '9')) {
            result = result * 10 + (line[c] - '0');
            ++c;
        }
        if (line[c] == '.') {
            ++c;
        }
        while ((line[c] >= '0') && (line[c] <= '9')) {
            result = result * 10 + (line[c] - '0');
            ++dot_pos;
            ++c;
        }
        result /= powf(10., (float)dot_pos);
        result *= sign;
        if (!isfinite(result)) {
            return false;
        }
        x[i] = result;
        ++i;
    }
    if (i < 7) {
        return false;
    }

    return true;
}


bool parse_line_double(char *line, double *x)
{
    uint8_t c = 0;
    int8_t i = 0;
    int8_t sign;
    double result; 
    uint8_t dot_pos;

    while (((line[c] != '\n') || (line[c] != EOF)) && (i < 8)) {
        result = 0;
        dot_pos = 0;
        sign = 1;
        while ((line[c] == ' ') || (line[c] == '\t')) {
            ++c;
        }
        if (line[c] == '-') {
            sign = -1;
            ++c;
        }
        if (((line[c] < '0') || (line[c] > '9')) && (line[c] != '.')) {
            return false;
        }
        while ((line[c] >= '0') && (line[c] <= '9')) {
            result = result * 10 + (line[c] - '0');
            ++c;
        }
        if (line[c] == '.') {
            ++c;
        }
        while ((line[c] >= '0') && (line[c] <= '9')) {
            result = result * 10 + (line[c] - '0');
            ++dot_pos;
            ++c;
        }
        result /= pow(10., (double)dot_pos);
        result *= sign;
        if (!isfinite(result)) {
            return false;
        }
        x[i] = result;
        ++i;
    }
    if (i < 7) {
        return false;
    }

    return true;
}

void print_results(float *a, float *b, float *c, double *d, double *f)
{
    printf("%-2s|%-13s|%-13s|%-13s|%-16s|%-17s\n", "i", "A[i]", "B[i]", "C[i]",
           "D[i]", "F[i]");

    int8_t i = 0; 

    for (; i < 79; i++) {
        printf("=");
    }
    printf("\n");
    for (i = 0; i < 8; i++) {
        printf("%-2d|%-13f|%-13f|%-13f|%-16f|%-17f\n", i, a[i], b[i], c[i],
               d[i], f[i]);
    }
}

